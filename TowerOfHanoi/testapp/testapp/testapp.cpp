// TowerOfHanoi.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
using namespace std;

vector<int> tower1, tower2, tower3;
void towers(int, char, char, char, vector<int>*, vector<int>*, vector<int>*);
void thePrinter();

int main()
{
	int num;

	cout << "Enter the number of discs:";
	cin >> num;
	for (int x = 0; x < num; x++)
	{
		tower1.push_back(num - x);
	}
	tower2.reserve(num);
	tower3.reserve(num);
	cout << "\nThe sequence of moves involved in the tower of hanoi are :\n";
	towers(num, 'A', 'C', 'B', &tower1, &tower2, &tower3);

	system("pause");
	return 0;
}

void towers(int num, char frompeg, char topeg, char auxpeg, vector<int> *tower1Param, vector<int> *tower2Param, vector<int> *tower3Param)
{
	if (num == 1)
	{
		cout << "\n Move disk 1 from peg " << frompeg << " te peg " << topeg << "\t";
		if (!tower1Param->empty())
		{
			tower2Param->push_back(tower1Param->back());
			tower1Param->pop_back();
			thePrinter();
		}
		return;
	}
	towers(num - 1, frompeg, auxpeg, topeg, tower1Param, tower3Param, tower2Param);
	cout << "\n Move disk " << num << " from peg " << frompeg << " to peg " << topeg << "\t";
	if (!tower1Param->empty())
	{
		tower2Param->push_back(tower1Param->back());
		tower1Param->pop_back();
		thePrinter();
	}
	towers(num - 1, auxpeg, topeg, frompeg, tower3Param, tower2Param, tower1Param);
}

void thePrinter()
{
	for (int x = 0; x < tower1.size(); x++)
	{
		cout << tower1.at(x);
	}
	cout << "\t";

	for (int x = 0; x < tower3.size(); x++)
	{
		cout << tower3.at(x);
	}
	cout << "\t";

	for (int x = 0; x < tower2.size(); x++)
	{
		cout << tower2.at(x);
	}
	cout << endl;
}

